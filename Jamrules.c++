rule CUIMain
{
	LINKLIBS on $(1) = -lmenu -lpanel -lncurses ;
	C++Main $(1) : $(2) ;
}

rule C++Main
{
	LINK on $(1) = g++ ;
	HDRS = $(TOP)/cpp ;
	Main $(1) : $(2) ;
}
