import subprocess
import argparse
import compiler
import os

def compileAllPythons():
	for top, dirnames, filenames in os.walk( 'cuipy' ):
		for filename in filenames:
			if filename.endswith( '.py' ):
				path = os.path.join( top, filename )
				compiler.compileFile( path )

def run( command ):
	print 'running: %s' % command
	subprocess.check_call( command, shell = True )

parser = argparse.ArgumentParser()
parser.add_argument( '--python', default = '/usr/include/python2.7', help = 'python installation, default = /usr/include/python2.7' )
arguments = parser.parse_args()

compileAllPythons()
run( 'g++ -fPIC -Icpp -I%s -shared -o cuipy/binaries/foundation.so cpp/PythonWrapper/pythonmodule.cpp /lib/x86_64-linux-gnu/libncurses.so.5 /usr/lib/x86_64-linux-gnu/libmenu.so.5' % arguments.python )
run( 'tar --exclude "*.py" -zcf cuipy.runnable.tgz cuipy/__init__.pyc cuipy/wrapper.pyc cuipy/binaries cuipy/menus' )
