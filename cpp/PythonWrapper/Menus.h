#ifndef PYTHONWRAPPER_MENUS_H
#define PYTHONWRAPPER_MENUS_H

#include "CUI/Menu/Menu.h"
#include "CUI/Menu/ItemSpecifications.h"
#include "CUI/WindowStackImplementation.h"

namespace PythonWrapper
{
namespace Menus
{

static PyObject * EscapePressedException;

typedef struct {
	PyObject_HEAD
	/* Type-specific fields go here. */
	bool initialized;
	CUI::Menu::Menu menu;

} PythonMenuObject;

static void PythonMenu_dealloc( PythonMenuObject * self );
static int PythonMenu_init( PythonMenuObject * self, PyObject * args, PyObject * kwargs );
static PyObject * PythonMenu_choice( PythonMenuObject * self );
static PyObject * PythonMenu_show( PythonMenuObject * self );
static PyObject * PythonMenu_close( PythonMenuObject * self );

static PyMethodDef PythonMenuObjectMethods[] = {
	{ "choice", (PyCFunction) PythonMenu_choice, METH_NOARGS, NULL },
	{ "show", (PyCFunction) PythonMenu_show, METH_NOARGS, NULL },
	{ "close", (PyCFunction) PythonMenu_close, METH_NOARGS, NULL },
	{NULL, NULL, 0, NULL}  /* Sentinel */
};

static PyTypeObject MenuTypeObject = {
	PyObject_HEAD_INIT(NULL)
	0,						 /*ob_size*/
	"menu.Menu",			 /*tp_name*/
	sizeof(PythonMenuObject), /*tp_basicsize*/
	0,						 /*tp_itemsize*/
	(destructor)PythonMenu_dealloc,						 /*tp_dealloc*/
	0,						 /*tp_print*/
	0,						 /*tp_getattr*/
	0,						 /*tp_setattr*/
	0,						 /*tp_compare*/
	0,						 /*tp_repr*/
	0,						 /*tp_as_number*/
	0,						 /*tp_as_sequence*/
	0,						 /*tp_as_mapping*/
	0,						 /*tp_hash */
	0,						 /*tp_call*/
	0,						 /*tp_str*/
	0,						 /*tp_getattro*/
	0,						 /*tp_setattro*/
	0,						 /*tp_as_buffer*/
	Py_TPFLAGS_DEFAULT,		/*tp_flags*/
	"Menu objects",		   /* tp_doc */
	0,					   /* tp_traverse */
	0,					   /* tp_clear */
	0,					   /* tp_richcompare */
	0,					   /* tp_weaklistoffset */
	0,					   /* tp_iter */
	0,					   /* tp_iternext */
	PythonMenuObjectMethods,	  /* tp_methods */
	0,						 /* tp_members */
	0,						 /* tp_getset */
	0,						 /* tp_base */
	0,						 /* tp_dict */
	0,						 /* tp_descr_get */
	0,						 /* tp_descr_set */
	0,						 /* tp_dictoffset */
	(initproc)PythonMenu_init,	  /* tp_init */
	0,						 /* tp_alloc */
	0,				 /* tp_new */
};

static void loadItems( PyObject * itemsList, CUI::Menu::ItemSpecifications & items )
{
	unsigned size = PyList_GET_SIZE( itemsList );
	for ( unsigned i = 0 ; i < size; ++ i ) {
		PyObject * itemTuple = PyList_GetItem( itemsList, i );
		PyObject * label = PyTuple_GetItem( itemTuple, 0 );
		PyObject * description = PyTuple_GetItem( itemTuple, 1 );
		char * labelString = PyString_AsString( label );
		char * descriptionString = PyString_AsString( description );
		items.add( labelString, descriptionString );
	}
}

static int PythonMenu_init( PythonMenuObject * self, PyObject * args, PyObject * kwargs )
{
	self->initialized = false;

	int height, width, startX, startY;
	const char * title;
	PyObject * itemsList;
	const char * frameColor = "NORMAL";
	const char * itemColor = "NORMAL";
	const char * selectedColor = "NORMAL_REVERSED";
	static char * kwlist[] = { 	(char*) "height", 
								(char*) "width", 
								(char*) "startY", 
								(char*) "startX", 
								(char*) "title", 
								(char*) "items", 
								(char*) "frameColor", 
								(char*) "itemColor", 
								(char*) "selectedColor", 
								NULL };

	if ( ! PyArg_ParseTupleAndKeywords( args, kwargs, "iiiisO|sss", kwlist,
									& height, 
									& width, 
									& startY, 
									& startX,
									& title,
									& itemsList,
									& frameColor,
									& itemColor,
									& selectedColor ) ) {
		return -1;
	}
	if ( ! PyList_Check( itemsList ) )
		return -1;

	CUI::Menu::ItemSpecifications items;
	loadItems( itemsList, items );

	try {
		new ( & self->menu ) CUI::Menu::Menu( 	height, 
												width, 
												startY, 
												startX, 
												title, 
												items,
												frameColor,
												itemColor,
												selectedColor );
	}
	catch ( CUI::Exception & exception ) {
		PyErr_SetString( FoundationException, exception.what() );
		return -1;
	}

	self->initialized = true;
	return 0;
}

static void PythonMenu_dealloc( PythonMenuObject * self )
{
	if ( ! self->initialized )
		return;
	self->menu.~Menu();
}

static PyObject * PythonMenu_choice( PythonMenuObject * self ) 
{
	int result;
	PyThreadState * _save;
	_save = PyEval_SaveThread();

	try {
		result = self->menu.choice();
	}
	catch ( CUI::Menu::Menu::ControlCPressedException & exception )
	{
		PyEval_RestoreThread( _save );
		PyErr_SetInterrupt();
		return NULL;
	}
	catch ( CUI::Menu::Menu::EscapePressedException & exception )
	{
		PyEval_RestoreThread( _save );
		PyErr_SetString( EscapePressedException, exception.what() );
		return NULL;
	}
	catch ( CUI::Exception & exception ) {
		PyEval_RestoreThread( _save );
		PyErr_SetString( FoundationException, exception.what() );
		return NULL;
	}
	PyEval_RestoreThread( _save );
	return Py_BuildValue( "i", result );
}

static PyObject * PythonMenu_show( PythonMenuObject * self ) 
{
	PyThreadState * _save;
	_save = PyEval_SaveThread();
	try {
		self->menu.show();
	}
	catch ( CUI::Exception & exception ) {
		PyEval_RestoreThread( _save );
		PyErr_SetString( EscapePressedException, exception.what() );
		return NULL;
	}
	PyEval_RestoreThread( _save );
	Py_RETURN_NONE;
}

static PyObject * PythonMenu_close( PythonMenuObject * self ) 
{
	PyThreadState * _save;
	_save = PyEval_SaveThread();
	try {
		self->menu.close();
	}
	catch ( CUI::Exception & exception ) {
		PyEval_RestoreThread( _save );
		PyErr_SetString( FoundationException, exception.what() );
		return NULL;
	}
	PyEval_RestoreThread( _save );
	Py_RETURN_NONE;
}

int initializeMenusClass( PyObject * module )
{
	Menus::MenuTypeObject.tp_new = PyType_GenericNew;
	if ( PyType_Ready( & Menus::MenuTypeObject ) < 0 )
		return -1;
	PyModule_AddObject( module, "Menu", (PyObject *) & Menus::MenuTypeObject );

	EscapePressedException = PyErr_NewException( (char*) "foundation.Menu.EscapePressedException", NULL, NULL);
	Py_INCREF( EscapePressedException );
	PyDict_SetItemString( Menus::MenuTypeObject.tp_dict, "EscapePressedException", EscapePressedException );
}

} // Menus
} // PythonWrapper
#endif // PYTHONWRAPPER_MENUS_H
