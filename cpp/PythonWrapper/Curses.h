#ifndef PYTHONWRAPPER_CURSES_H
#define PYTHONWRAPPER_CURSES_H

#include "CUI/Curses/Curses.h"

namespace PythonWrapper
{
namespace Curses 
{

PyObject * initialize( PyObject * self, PyObject * args )
{
	EXCEPTION_WRAPPER( CUI::Curses::Curses::initialize() );
	Py_RETURN_NONE;
}

PyObject * finalize( PyObject * self, PyObject * args )
{
	EXCEPTION_WRAPPER( CUI::Curses::Curses::finalize() );
	Py_RETURN_NONE;
}

PyObject * suspend( PyObject * self, PyObject * args )
{
	EXCEPTION_WRAPPER( CUI::Curses::Curses::suspend() );
	Py_RETURN_NONE;
}

PyObject * resume( PyObject * self, PyObject * args )
{
	EXCEPTION_WRAPPER( CUI::Curses::Curses::resume() );
	Py_RETURN_NONE;
}


} // Curses
} // PythonWrapper
#endif // PYTHONWRAPPER_CURSES_H
