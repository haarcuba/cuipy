#include <Python.h>

namespace PythonWrapper {

static PyObject * FoundationException;

}

#define EXCEPTION_WRAPPER( X )\
	try {\
		X;\
	}\
	catch ( CUI::Exception & exception ) {\
		PyErr_SetString( FoundationException, exception.what() );\
		return NULL;\
	}

#include "PythonWrapper/Curses.h"
#include "PythonWrapper/Menus.h"

namespace PythonWrapper 
{

static PyMethodDef moduleMethods[] = {
	{"initialize", Curses::initialize, METH_NOARGS, "initialize curses" },
	{"finalize", Curses::finalize, METH_NOARGS, "finalize curses" },
	{"suspend", Curses::suspend, METH_NOARGS, "suspend curses" },
	{"resume", Curses::resume, METH_NOARGS, "resume curses" },
	{ NULL, NULL, 0, NULL }
};


}


PyMODINIT_FUNC
initfoundation(void)
{
	PyObject *module;
	module = Py_InitModule( "foundation", PythonWrapper::moduleMethods );
	if (module == NULL)
		return;

	if ( PythonWrapper::Menus::initializeMenusClass( module ) < 0 )
		return;

	PythonWrapper::FoundationException = PyErr_NewException( (char*) "foundation.Exception", NULL, NULL);
	Py_INCREF( PythonWrapper::FoundationException );
	PyModule_AddObject( module, "Exception", PythonWrapper::FoundationException );
}
