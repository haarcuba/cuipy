#ifndef COMMON_ASSERT_H
#define COMMON_ASSERT_H

#include "Common/Formatter.h"
#include "Common/Exception.h"

class AssertionError : public Exception {
public:
	explicit AssertionError( char const * message ) :
		::Exception( message ) 
	{
	}

};

#define ASSERT(x) if ( ! (x) ) throw AssertionError( Formatter( "AssertionError! at %s:%d", __FILE__, __LINE__ ) );

#endif // COMMON_ASSERT_H
