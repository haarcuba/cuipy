#ifndef COMMON_LOGGING_H
#define COMMON_LOGGING_H

#include <stdarg.h>
#include <iostream>
#include "Common/Formatter.h"

// Uncomment the line below for C++ logging
//#define CPP_DEBUG

#ifdef CPP_DEBUG

#	define LOGGING( level, argumentsForFormatterInParanthesis )\
		std::cerr << level << ": " << Formatter argumentsForFormatterInParanthesis << " (" << __FILE__ << ":" << __LINE__ << ")\n";

#else

#	define LOGGING( level, argumentsForFormatterInParanthesis )

#endif // CPP_DEBUG

#define	LOGGING_ERROR( argumentsForFormatterInParanthesis )	LOGGING( "ERROR", argumentsForFormatterInParanthesis )
#define	LOGGING_WARNING( argumentsForFormatterInParanthesis )	LOGGING( "WARNING", argumentsForFormatterInParanthesis )
#define	LOGGING_INFO( argumentsForFormatterInParanthesis )	LOGGING( "INFO", argumentsForFormatterInParanthesis )
#define	LOGGING_DEBUG( argumentsForFormatterInParanthesis )	LOGGING( "DEBUG", argumentsForFormatterInParanthesis )

#endif // COMMON_LOGGING_H
