#ifndef COMMON_EXCEPTION_H
#define COMMON_EXCEPTION_H

#include <stdexcept>

class Exception : public std::runtime_error {
public: 
	explicit Exception( const char * message ) :
		std::runtime_error( message )
	{
	}
};

#endif // COMMON_EXCEPTION_H
