#ifndef COMMON_FORMATTER_H
#define COMMON_FORMATTER_H

#include <cstdio>

class Formatter {
public:
	explicit Formatter( const char * format, ... )
	{
		va_list ap;
		va_start( ap, format );
		std::vsnprintf( _string, SIZE, format, ap );
		va_end( ap );
	}

	const char * string() { return _string; }

	operator const char * ()
	{
		return _string;
	}

private:
	enum { 
		SIZE = 1000
	};
	char _string[ SIZE ];

};

#endif // COMMON_FORMATTER_H
