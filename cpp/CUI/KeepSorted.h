#ifndef CUI_KEEPSORTED_H
#define CUI_KEEPSORTED_H

#include <map>
#include "CUI/Exception.h"

namespace CUI
{

template < typename Data, typename Key >
class KeepSorted {
public:
	explicit KeepSorted() 
	{
	}

	void add( Data const & data, Key const & key )
	{
		_map[ key ] = data;
		_inverseMap[ data ] = key;
	}

	void remove( Data const & data )
	{
		typename InverseMap::iterator inversePosition = _inverseMap.find( data );
		if ( inversePosition == _inverseMap.end() )
			return;
		Key key = inversePosition->second;
		_inverseMap.erase( inversePosition );
		_map.erase( key );
	}

	void update( Data const & data, Key const & key )
	{
		remove( data );
		add( data, key );
	}

	class Iterator;

	Iterator begin()
	{
		return Iterator( _map.begin() );
	}
	Iterator end()
	{
		return Iterator( _map.end() );
	}

	unsigned size() const { return _map.size(); }

private:
	KeepSorted( KeepSorted const & );

	typedef std::map< Key, Data > Map;
	typedef std::map< Data, Key > InverseMap;
	Map 						_map;
	InverseMap					_inverseMap;
};

} // CUI

#include "CUI/KeepSortedIterator.h"
#endif // CUI_KEEPSORTED_H
