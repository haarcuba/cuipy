#ifndef CUI_WINDOWSTACKIMPLEMENTATION_H
#define CUI_WINDOWSTACKIMPLEMENTATION_H

namespace CUI
{

void WindowStack::add( Window & window )
{
	_sortedWindows.add( & window, _high );
	++ _high;
}

void WindowStack::putOnTop( Window & window )
{
	_sortedWindows.update( & window, _high );
	++ _high;
}

void WindowStack::putOnBottom( Window & window ) 
{
	_sortedWindows.update( & window, _low );
	-- _low;
}

void WindowStack::hide( Window & window )
{
	virtualRefreshBackgroundWindow();
	_sortedWindows.remove( & window );
}

void WindowStack::show( Window & window )
{
	add( window );
}

void WindowStack::refresh()
{
	SortedWindows::Iterator i;
	for ( i = _sortedWindows.begin(); i != _sortedWindows.end(); ++i )
		( * i )->virtualRefresh();

	Curses::Curses::updateScreen();
}

WindowStack WindowStack::instance;

} // CUI
#endif // CUI_WINDOWSTACKIMPLEMENTATION_H
