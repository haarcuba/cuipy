#include <string>
#include <stdarg.h>
#include <iostream>
#include <vector>
#include "CUI/KeepSorted.h"
#include "Common/Formatter.h"

class TestAssertionError {
public:
	TestAssertionError( std::string const & text ) :
		_text( text )
	{
	}

	std::string const & what() const { return _text; }
private:
	std::string _text;
};

#define TS_ASSERT( condition )\
{\
	if ( ! ( condition ) ) {\
		throw TestAssertionError( Formatter( "ERROR on %s:%d", __FILE__, __LINE__ ).string() );\
	}\
}

using namespace CUI;

class Test_KeepSorted {
	public:
	typedef KeepSorted< std::string, int > Tested;

	Test_KeepSorted()
	{
		test_BasicOperation();
		test_Update();
		test_ElementNotFound_UpdateAddsElement();
		test_Remove();
	}

	void test_BasicOperation() 
	{
		Tested tested;
		TS_ASSERT( tested.size() == 0 );
		tested.add( "ham", 1 );
		tested.add( "jam", 2 );
		tested.add( "spam", 3 );
		TS_ASSERT( tested.size() == 3 );

		Tested::Iterator i;
		std::vector< std::string > strings;
		for( i = tested.begin(); i != tested.end(); ++i )
			strings.push_back( * i );

		TS_ASSERT( strings[ 0 ] == "ham" );
		TS_ASSERT( strings[ 1 ] == "jam" );
		TS_ASSERT( strings[ 2 ] == "spam" );
	}

	void test_Update()
	{
		Tested tested;
		TS_ASSERT( tested.size() == 0 );
		tested.add( "ham", 1 );
		tested.add( "jam", 2 );
		tested.add( "spam", 3 );
		TS_ASSERT( tested.size() == 3 );

		Tested::Iterator i;
		std::vector< std::string > strings;
		for( i = tested.begin(); i != tested.end(); ++i )
			strings.push_back( * i );

		TS_ASSERT( strings[ 0 ] == "ham" );
		TS_ASSERT( strings[ 1 ] == "jam" );
		TS_ASSERT( strings[ 2 ] == "spam" );

		tested.update( "ham", 4 );
		strings.clear();
		for( i = tested.begin(); i != tested.end(); ++i )
			strings.push_back( * i );

		TS_ASSERT( strings[ 0 ] == "jam" );
		TS_ASSERT( strings[ 1 ] == "spam" );
		TS_ASSERT( strings[ 2 ] == "ham" );
	}

	void test_ElementNotFound_UpdateAddsElement()
	{
		Tested tested;
		const char * NOT_THERE = "potato";
		tested.update( NOT_THERE, 7 );
		Tested::Iterator i = tested.begin();
		TS_ASSERT( * i == "potato" );
	}

	void test_Remove() 
	{
		Tested tested;
		TS_ASSERT( tested.size() == 0 );
		tested.add( "ham", 1 );
		tested.add( "jam", 2 );
		tested.add( "spam", 3 );
		TS_ASSERT( tested.size() == 3 );

		tested.remove( "ham" );

		std::vector< std::string > strings;
		Tested::Iterator i;
		for( i = tested.begin(); i != tested.end(); ++i )
			strings.push_back( * i );

		TS_ASSERT( tested.size() == 2 );
		TS_ASSERT( strings[ 0 ] == "jam" );
		TS_ASSERT( strings[ 1 ] == "spam" );
	}
};


main()
{
	try {
		Test_KeepSorted();
	}
	catch ( TestAssertionError & e ) {
		std::cout << e.what() << "\n";
		throw ;
	}
}
