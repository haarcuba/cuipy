#include <iostream>
#include "CUI/Curses/Curses.h"
#include "Common/Formatter.h"
#include "CUI/WindowStacking.h"
#include <unistd.h>

using namespace CUI;

void scrollText( Window & window, WindowStack & windowStack )
{
	for ( unsigned i = 0; i < 15; ++i ) {
		window.print( Formatter( "%d this is line %d\n", i, i ).string() );
		windowStack.refresh();
		usleep( 100 * 1000 );
	}
}

void go()
{
	Window window1( 10, 70, 2, 2 );
	Window window2( 10, 70, 7, 7 );
	WindowStack & windowStack = WindowStack::instance;

	window1.enableScrolling();
	window2.enableScrolling();

	window1.title( "Window 1" );
	window2.title( "Window 2" );

	windowStack.refresh();

	window2.print( "Press a key to start scrolling on other window\n" );
	window2.inputCharacter();
	
	scrollText( window1, windowStack );

	window2.print( "press a key to hide window 2\n" );
	window2.inputCharacter();
	window2.hide();
	windowStack.refresh();
	window1.print( "press another key to make window 2 reappear behind me\n" );
	window1.inputCharacter();

	window2.show();
	window1.putOnTop();
	windowStack.refresh();

	scrollText( window2, windowStack );

	window1.print( "press a key to put window2 on top again\n" );
	window1.inputCharacter();
	window2.putOnTop();
	windowStack.refresh();
	window2.print( "press key for another window to appear\n" );
	window2.inputCharacter();

	Window window3( 10, 70, 4, 40 );
	window3.title( "Window 3" );
	window3.print( "Press a key to put me on bottom\n" );
	windowStack.refresh();
	window3.inputCharacter();
	windowStack.putOnBottom( window3 );
	windowStack.refresh();

	window2.print( "Press a key to make the new window on top\n" );
	windowStack.refresh();
	window2.inputCharacter();

	window3.putOnTop();
	window3.print( "Press a key to exit :)" );
	windowStack.refresh();
	window3.inputCharacter();
}

main()
{
	Curses::Curses::initialize();
	try {
		go();
	}
	catch ( CUI::Exception & e ) {
		Curses::Curses::finalize();
		std::cerr << "Exception: " << e.what() << "\n";
		throw ;
	}
	Curses::Curses::finalize();
	std::cout << "OK\n";
}
