#include <iostream>
#include <cstdarg>
#include "CUI/WindowStacking.h"
#include "CUI/Menu/Menu.h"

void secondMenu( CUI::Window & window )
{
	using namespace CUI;
	Menu::ItemSpecifications items;
	items.add( "1", "option A" );
	items.add( "2", "option B" );
	items.add( "3", "option C" );
	Menu::Menu menu( 10, 50, 4, 4, "Menu II!", items );

	int choice = menu.popup();
	window.print( Formatter( "second menu choice: %s:%s\n", items[ choice ].name.c_str(), items[ choice ].description.c_str() ) );
	WindowStack::instance.refresh();
}

void go()
{
	using namespace CUI;
	CUI::WindowStack & windowStack = CUI::WindowStack::instance;
	CUI::Window window( 10, 80, 5, 5 );
	window.title( "Main Window" );
	window.print( "press a key for the menu\n" );
	windowStack.refresh();
	window.inputCharacter();

	Menu::ItemSpecifications items;
	items.add( "a)", "The first item" );
	items.add( "b)", "The second item" );
	items.add( "c)", "The third item" );
	items.add( "d)", "Another Menu..." );
	Menu::Menu menu( 10, 50, 2, 2, "My Menu!", items );
	
	menu.show();
	int choice = menu.choice();
	if ( choice == 3 )
		secondMenu( window );

	menu.close();

	windowStack.refresh();
	window.print( Formatter( "you chose %s:%s\n", items[ choice ].name.c_str(), items[ choice ].description.c_str() ) );
	window.print( "press key to exit\n" );
	windowStack.refresh();
	window.inputCharacter();
}

main()
{
	using namespace CUI;
	Curses::Curses::initialize();
	try {
		go();
	}
	catch ( ... )
	{
		Curses::Curses::finalize();
		throw ;
	}
	Curses::Curses::finalize();
	std::cout << "OK\n";
}
