#include "CUI/Curses/Curses.h"
#include "CUI/Curses/Window.h"
#include "CUI/Curses/Panel.h"
#include "CUI/Curses/Panels.h"

main()
{
	using namespace CUI::Curses;
	Curses::initialize();
	Window window1( 10, 70, 2, 2 );
	Window window2( 10, 70, 4, 4 );
	Panel panel1( window1 );
	Panel panel2( window2 );

	panel1.window().box();
	panel1.window().print( "panel 1" );
	panel2.window().box();
	panel2.window().print( "panel 2" );
	Panels::refresh();

	panel1.window().inputCharacter();

	panel1.putOnTop();
	Panels::refresh();

	panel1.window().inputCharacter();

	panel2.putOnTop();
	Panels::refresh();
	panel1.window().inputCharacter();

	Curses::finalize();
}
