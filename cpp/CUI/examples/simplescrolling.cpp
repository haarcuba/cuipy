#include <iostream>
#include "CUI/Curses/Curses.h"
#include "CUI/Curses/Window.h"
#include "Common/Formatter.h"
#include <unistd.h>

main()
{
	using namespace CUI::Curses;
	Curses::initialize();
	try {
		Window window( 10, 70, 2, 2 );
		window.enableScrolling();
		window.box();
		for ( unsigned i = 0; i < 50; ++i )  {
			window.print( Formatter( "this is line %d\n", i ).string() );
			window.refresh();
			usleep( 100 * 1000 );
		}
	}
	catch ( Exception & e ) {
		Curses::finalize();
		std::cout << "exception thrown! " << e.what() << "\n";
		throw ;
	}
	catch ( std::exception & e ) {
		Curses::finalize();
		std::cout << "exception thrown! but not from CUI " << e.what() << "\n";
		throw ;
	}
	Curses::finalize();
	std::cout << "OK!\n";
}
