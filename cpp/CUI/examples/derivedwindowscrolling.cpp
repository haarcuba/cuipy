#include <iostream>
#include <unistd.h>
#include "CUI/Curses/Curses.h"
#include "CUI/Curses/Window.h"
#include "Common/Formatter.h"

main()
{
	using namespace CUI::Curses;
	Curses::initialize();
	Curses::showCursor();
	Window frame( 10, 70, 2, 2 );
	Window inside( frame, 8, 68, 1, 1 );
	inside.enableScrolling();
	frame.box();

	inside.print( "I'm showing the cursor so you see what's going on " );
	inside.refresh();
	sleep( 2 );

	inside.print( "let's go!\n" );
	inside.refresh();
	sleep( 1 );

	for ( unsigned i = 1; i <= 15; ++i )  {
		inside.print( Formatter( "this is line %d", i ) );
		inside.refresh();
		usleep( 500 * 1000 );
		inside.print( " here comes the newline\n" );
		inside.refresh();
		usleep( 500 * 1000 );
	}
	sleep( 2 );
	inside.print( "\nOK enough scrolling for today :) " );
	inside.refresh();
	sleep( 2 );

	Curses::finalize();
	std::cout << "OK!\n";
}
