#include <iostream>
#include <cstdarg>
#include "CUI/WindowStacking.h"
#include "CUI/Menu/Menu.h"

void go()
{
	using namespace CUI;
	CUI::WindowStack & windowStack = CUI::WindowStack::instance;
	Window window( 10, 70, 7, 7 );
	window.title( "Hi there" );
	window.print( "press key for menu, then curses will be suspended\n" );
	windowStack.refresh();
	window.inputCharacter();
	Menu::ItemSpecifications items;
	items.add( "0", "item zero" );
	items.add( "1", "item one" );
	items.add( "2", "item two" );
	Menu::Menu menu( 10, 20, 5, 5, "The Menu", items );
	int choice( 0 );
	choice = menu.popup();

	Curses::Curses::suspend();
	std::cout 	<< "you chose \"" << items[ choice ].description 
				<< "\"\n" ;
	std::cout << "I will now print some lines with std::cout\n";
	for ( int i = 0; i < 10; ++i ) {
		std::cout << "line " << i << "\n";
	}
	std::cout << "Enter something to return to curses mode\n";
	std::cin >> choice;
	window.print( "press another key to display menu again\n" );
	window.inputCharacter();
	windowStack.refresh();
	Curses::Curses::resume();
	choice = menu.popup();
	window.print( "press any key to exit\n" );
	windowStack.refresh();
	window.inputCharacter();
}

main()
{
	using namespace CUI;
	Curses::Curses::initialize();
	try {
		go();
	}
	catch ( ... ) {
		Curses::Curses::finalize();
		throw ;
	}
	Curses::Curses::finalize();
	std::cout << "OK\n";
}
