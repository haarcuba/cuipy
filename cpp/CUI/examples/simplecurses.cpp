#include "CUI/Curses/Curses.h"
#include "CUI/Curses/Window.h"

main()
{
	using namespace CUI::Curses;
	Curses::initialize();
	Window window( 10, 70, 2, 2 );
	window.box();
	window.print( "This is a string" );
	window.inputCharacter();
	Curses::finalize();
}
