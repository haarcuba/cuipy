#include <iostream>
#include <cstdarg>
#include "CUI/WindowStacking.h"
#include "CUI/Menu/Menu.h"

void go()
{
	using namespace CUI;
	CUI::WindowStack & windowStack = CUI::WindowStack::instance;
	CUI::Window window( 10, 80, 5, 5, "RED_ON_BLACK" );
	window.title( "Main Window" );
	window.setColor( "YELLOW_ON_BLACK" );
	window.print( "press a key for the menu\n" );
	windowStack.refresh();
	window.inputCharacter();

	Menu::ItemSpecifications items;
	items.add( "a)", "The first item" );
	items.add( "b)", "The second item" );
	items.add( "c)", "The third item" );

	Menu::Menu menu(	10,
						50,
						2,
						2,
						"My Menu!",
						items,
						"CYAN_ON_BLACK",
						"GREEN_ON_BLACK",
						"BLACK_ON_GREEN" );
	int choice = menu.popup();
	windowStack.refresh();
	window.print( Formatter( 	"you chose \"%s %s\"\n", 
								items[ choice ].name.c_str(), 
								items[ choice ].description.c_str() ) );
	window.print( "press key to exit\n" );
	windowStack.refresh();
	window.inputCharacter();
}

main()
{
	using namespace CUI;
	Curses::Curses::initialize();
	try {
		go();
	}
	catch ( ... ) {
		Curses::Curses::finalize();
		throw ;
	}
	Curses::Curses::finalize();
	std::cout << "OK\n";
}
