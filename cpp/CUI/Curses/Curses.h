#ifndef CUI_CURSES_CURSES_H
#define CUI_CURSES_CURSES_H

#include <curses.h>
#include "CUI/Exception.h"
#include "Common/Assert.h"
#include "CUI/Curses/Colors.h"
#include "Common/Logging.h"

namespace CUI
{
namespace Curses
{

class Curses {
public:
	static void initialize()
	{
		if ( _initialized )
			return;
		checkForError( initscr(), "can not initialize curses" );
		checkForError( start_color(), "monitor has no colors, and this is the 21st century, get real" );
		checkForError( raw(), "can not set raw mode" );
		checkForError( keypad( stdscr, true ), "can not set keypad on stdscrr" );
		checkForError( noecho(), "can not turn off echo" );
		const int ESCAPE_DELAY_MILLISECONDS = 100;
		LOGGING_INFO(( "escape delay was %d ms, setting to %d ms", get_escdelay(), ESCAPE_DELAY_MILLISECONDS ));
		set_escdelay( ESCAPE_DELAY_MILLISECONDS );
		hideCursor();
		Colors::initialize();
		_initialized = true;
	}

	static void virtualRefreshMainWindow()
	{
		int result;
		result = touchwin( stdscr );
		Curses::Curses::checkForError( result, "can not touch main window" );
		result = wnoutrefresh( stdscr );
		Curses::Curses::checkForError( result, "can not refresh main window" );
	}

	static void updateScreen()
	{
		int result = doupdate();
		checkForError( result, "can not update screen" );
	}

	static void suspend()
	{
		def_prog_mode();
		checkForError( endwin(), "can not end curses in Curses::suspend()" );
	}

	static void resume()
	{
		reset_prog_mode();
		updateScreen();
	}

	static void hideCursor()
	{
		int result = curs_set( 0 );
		checkForError( result, "can not hide cursor" );
	}

	static void showCursor()
	{
		int result = curs_set( 1 );
		checkForError( result, "can not show cursor" );
	}

	static void finalize()
	{
		ASSERT( _initialized );
		showCursor();
		checkForError( echo(), "can not enable echo" );
		checkForError( keypad( stdscr, false ), "can not reset keypad on stdscr" );
		checkForError( noraw(), "can not turn off raw mode" );
		checkForError( endwin(), "can not end curses" );
	}

	static void checkForError( int result, const char * message )
	{
		if ( result == ERR )
			throw Exception( message );
	}

	template <typename T>
	static void checkForError( const T * pointer, const char * message )
	{
		if ( pointer == NULL )
			throw Exception( message );
	}

private:
	Curses();

	static bool _initialized;

};

bool Curses::_initialized = false;

} // Curses
} // CUI
#endif // CUI_CURSES_CURSES_H
