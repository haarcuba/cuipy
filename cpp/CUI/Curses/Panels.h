#ifndef CUI_CURSES_PANELS_H
#define CUI_CURSES_PANELS_H

#include <panel.h>
namespace CUI
{
namespace Curses
{

class Panels {
public:
	static void refresh()
	{
		update_panels();
		doupdate();
	}
private:
	Panels();
};

} // Curses
} // CUI
#endif // CUI_CURSES_PANELS_H
