#ifndef CUI_CURSES_WINDOW_H
#define CUI_CURSES_WINDOW_H

#include <curses.h>
#include "Common/Assert.h"
#include "CUI/Curses/Curses.h"

namespace CUI
{
namespace Curses
{

class Window {
public:
	explicit Window( Window & parent, int height, int width, int startY, int startX ) :
		_window( derwin( parent.handle(), height, width, startX, startY ) ),
		_parent( & parent )
	{
		Curses::checkForError( _window, "can not create window" );
	}
		
	explicit Window( int height, int width, int startY, int startX ) :
		_window( newwin( height, width, startY, startX ) ),
		_parent( NULL )
	{
		Curses::checkForError( _window, "can not create window" );
	}

	~Window()
	{
		delwin( _window );
	}

	void setColor( Colors::Color const & color )
	{
		int result = wstandend( _window );
		Curses::checkForError( result, "can not unset attributes" );
		result = wattron( _window, Colors::colorPair( color ) );
		Curses::checkForError( result, "can not turn on attribute" );
	}

	int inputCharacter()
	{
		ASSERT( _window != NULL );
		return wgetch( _window );
	}

	void print( const char * string )
	{
		ASSERT( _window != NULL );
		int result = wprintw( _window, string );
		Curses::checkForError( result, "can not write to window" );
	}

	void print( const char * string, int y, int x )
	{
		int result = mvwprintw( _window, y, x, string );
		Curses::checkForError( result, "can not write (mvprintw) to window" );
	}

	void box()
	{
		ASSERT( _window != NULL );
		::box( _window, 0, 0 );
	}

	void refresh()
	{
		ASSERT( _window != NULL );
		if ( _parent != NULL ) {
			_parent->touch();
			_parent->refresh();
		}
		int result = wrefresh( _window );
		Curses::checkForError( result, "Can not refresh window" );
	}

	void virtualRefresh()
	{
		ASSERT( _window != NULL );
		if ( _parent != NULL ) {
			_parent->touch();
			_parent->virtualRefresh();
		}
		int result = wnoutrefresh( _window );
		Curses::checkForError( result, "Can not noutrefresh window" );
	}

	void touch()
	{
		ASSERT( _window != NULL );
		int result = touchwin( _window );
		Curses::checkForError( result, "Can not touch window" );
	}

	WINDOW * handle() const
	{
		ASSERT( _window != NULL );
		return _window;
	}

	void enableScrolling()
	{
		ASSERT( _window != NULL );
		int result = scrollok( _window, true );
		Curses::checkForError( result, "can not enable scrolling (scrollok)" );
	}

	void disableScrolling()
	{
		ASSERT( _window != NULL );
		int result = scrollok( _window, false );
		Curses::checkForError( result, "can not disable scrolling" );
	}

	void enableSpecialKeys()
	{
		int result = keypad( _window, true );
		Curses::checkForError( result, "could not set keypad on window" );
	}

private:
	Window( const Window & );
	WINDOW * _window;
	Window * _parent;
};

} // Curses
} // CUI
#endif // CUI_CURSES_WINDOW_H
