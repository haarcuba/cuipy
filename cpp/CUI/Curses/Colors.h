#ifndef CUI_CURSES_COLORS_H
#define CUI_CURSES_COLORS_H

#include <map>
#include "Common/Formatter.h"

namespace CUI
{
namespace Curses
{

class Colors {
public:
	typedef std::string Color;

	static int colorPair( Color const & color )
	{
		if ( _map.find( color ) == _map.end() )
			throw Exception( Formatter( "no color \"%s\"", color.c_str() ) );
		return COLOR_PAIR( _map[ color ] );
	}

	static void initialize()
	{
		if ( _map.size() > 0 )
			return;
		_map[ "NORMAL" ] = 0;
		initializeColor( "NORMAL_REVERSED", COLOR_BLACK, COLOR_WHITE );
		initializeColor( "RED_ON_BLACK", COLOR_RED, COLOR_BLACK );
		initializeColor( "WHITE_ON_BLACK", COLOR_WHITE, COLOR_BLACK );
		initializeColor( "YELLOW_ON_BLACK", COLOR_YELLOW, COLOR_BLACK );
		initializeColor( "GREEN_ON_BLACK", COLOR_GREEN, COLOR_BLACK );
		initializeColor( "BLACK_ON_GREEN", COLOR_BLACK, COLOR_GREEN );
		initializeColor( "BLUE_ON_BLACK", COLOR_BLUE, COLOR_BLACK );
		initializeColor( "CYAN_ON_BLACK", COLOR_CYAN, COLOR_BLACK );
	}

private:

	typedef std::map< Color, short > Map;
	static Map _map;

	static void initializeColor( Color const & color, short foreground, short background )
	{
		unsigned nextColorIndex = _map.size() + 1;
		assertCursesNeedsPositiveColorIndex( nextColorIndex );
		int result = init_pair( (short) nextColorIndex, foreground, background );
		checkForError( result, Formatter( "could not initialize color \"%s\"", color.c_str() ) );
		_map[ color ] = (short) nextColorIndex;
	}

	static void assertCursesNeedsPositiveColorIndex( unsigned colorIndex )
	{
		ASSERT( colorIndex > 0 );
	}

	static void checkForError( int result, const char * message )
	{
		if ( result == ERR )
			throw Exception( message );
	}

};

Colors::Map Colors::_map;

} // Curses
} // CUI
#endif // CUI_CURSES_COLORS_H
