#ifndef CUI_CURSES_PANEL_H
#define CUI_CURSES_PANEL_H

#include <panel.h>
#include "Common/Assert.h"

namespace CUI
{
namespace Curses
{

class Panel {
public:
	explicit Panel( Window & window ) :
		_panel( new_panel( window.handle() ) ),
		_window( window )
	{
		Curses::checkForError( _panel, "could not create panel" );
	}
	
	~Panel()
	{
		ASSERT( _panel != NULL );
		del_panel( _panel );
	}

	void hide()
	{
		ASSERT( _panel != NULL );
		int result = hide_panel( _panel );
		Curses::checkForError( result, "could not hide panel" );
	}

	void show()
	{
		ASSERT( _panel != NULL );
		int result = show_panel( _panel );
		Curses::checkForError( result, "could not show panel" );
	}

	void putOnTop()
	{
		ASSERT( _panel != NULL );
		int result = top_panel( _panel );
		Curses::checkForError( result, "could not put panel on top" );
	}

	void putOnBottom()
	{
		ASSERT( _panel != NULL );
		int result = bottom_panel( _panel );
		Curses::checkForError( result, "could not put panel on bottom" );
	}

	Window & window()
	{
		ASSERT( _panel != NULL );
		return _window;
	}
	
private:
	Panel( const Panel & );

	PANEL 	* _panel;
	Window 	& _window;
};

} // Curses
} // CUI
#endif // CUI_CURSES_PANEL_H
