#ifndef CUI_KEEPSORTEDITERATOR_H
#define CUI_KEEPSORTEDITERATOR_H

namespace CUI
{

template < typename Data, typename Key >
class KeepSorted< Data, Key >::Iterator {
private:
	typedef KeepSorted< Data, Key >	KeepSortedType;
	typedef typename KeepSortedType::Map::iterator MapIterator;
public:
	explicit Iterator( MapIterator mapIterator ) :
		_mapIterator( mapIterator )
	{
	}

	explicit Iterator()
	{
	}

	Iterator( const Iterator & other ) :
		_mapIterator( other._mapIterator )
	{
	}

	Iterator operator ++()
	{
		_mapIterator ++;
	}

	Data const & operator * ()
	{
		return _mapIterator->second;
	}

	bool operator == ( const Iterator & other ) const
	{
		return _mapIterator == other._mapIterator;
	}

	bool operator != ( const Iterator & other ) const
	{
		return _mapIterator != other._mapIterator;
	}

private:
	MapIterator _mapIterator;
};

} // CUI
#endif // CUI_KEEPSORTEDITERATOR_H
