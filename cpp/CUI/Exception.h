#ifndef CUI_EXCEPTION_H
#define CUI_EXCEPTION_H

#include <string>
#include "Common/Exception.h"

namespace CUI
{

class Exception : public ::Exception {
public:
	explicit Exception( const char * message ) :
		::Exception( message )
	{
	}
};


} // CUI
#endif // CUI_EXCEPTION_H
