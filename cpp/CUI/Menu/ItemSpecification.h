#ifndef CUI_MENU_ITEMSPECIFICATION_H
#define CUI_MENU_ITEMSPECIFICATION_H

namespace CUI
{
namespace Menu
{

struct ItemSpecification {
	explicit ItemSpecification(	const std::string & name,
								const std::string & description ) :
		name( name ),
		description( description )
	{
	}

	std::string name;
	std::string description;
};

} // Menu
} // CUI
#endif // CUI_MENU_ITEMSPECIFICATION_H
