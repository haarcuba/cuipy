#ifndef CUI_MENU_ITEMSPECIFICATIONS_H
#define CUI_MENU_ITEMSPECIFICATIONS_H

#include "CUI/Menu/ItemSpecification.h"

namespace CUI
{
namespace Menu
{

class ItemSpecifications : public std::vector< ItemSpecification > {
public:
	void add(	std::string const & name,
				std::string const & description )
	{
		push_back( ItemSpecification( name, description ) );
	}
};

} // Menu
} // CUI
#endif // CUI_MENU_ITEMSPECIFICATIONS_H
