#ifndef CUI_MENU_MENU_H
#define CUI_MENU_MENU_H

#include <vector>
#include <map>
#include <string>
#include <menu.h>
#include "CUI/Window.h"
#include "CUI/Menu/ItemSpecifications.h"
#include "Common/Formatter.h"
#include "Common/Assert.h"
#include "Common/Logging.h"

namespace CUI
{
namespace Menu
{

class Menu {
public:

	class ControlCPressedException : public Exception {
	public:
		explicit ControlCPressedException() :
			Exception( "Control-C pressed" )
		{
		}
	};

	class EscapePressedException : public Exception {
	public:
		explicit EscapePressedException() :
			Exception( "Escape pressed" )
		{
		}
	};

	explicit Menu(	int height,
					int width,
					int startY,
					int startX,
					const std::string & title,
					ItemSpecifications const & items,
					std::string const & frame = "NORMAL",
					std::string const & item = "NORMAL",
					std::string const & selected = "NORMAL_REVERSED" ) :
		_menu( NULL ),
		_window( height, width, startY, startX, frame ),
		_closed( true ),
		_choice( 0 )
	{
		_window.close();
		_window.title( title );
		createCursesItems( items );
		_menu = new_menu( _itemHandles );
		Curses::Curses::checkForError( _menu, "could not allocate menu" );
		int result = set_menu_win( _menu, _window.outerWindow().handle() );
		checkForError( result, "could not set menu window" );
		result = set_menu_sub( _menu, _window.innerWindow().handle() );
		checkForError( result, "could not set menu sub window" );
		setupColors( item, selected );
		_window.enableSpecialKeys();
	}

	~Menu()
	{
		ASSERT( _itemNames.size() == _itemDescriptions.size() );
		free_menu( _menu );
		for ( unsigned i = 0; i < _itemNames.size(); ++i )
			free_item( _itemHandles[ i ] );
		delete [] _itemHandles;
	}

	int popup()
	{
		ASSERT( _menu != NULL );
		show();
		int result = choice();
		close();
		return result;
	}

	int choice()
	{
		const int _KEY_ENTER = 0x0A;
		const int _KEY_CTRL_C = 0x03;
		const int _KEY_ESCAPE = 0x1B;
		int input = _window.inputCharacter();

		while ( input != _KEY_ENTER ) {
			switch( input ) {
				case KEY_DOWN:
					if ( _choice < itemCount() - 1  ) {
						driveMenu( REQ_DOWN_ITEM );
						++ _choice;
					}
					else {
						_choice = 0;
						driveMenu( REQ_FIRST_ITEM );
					}
					break;
				case KEY_UP:
					if ( _choice - 1 >= 0 ) {
						driveMenu( REQ_UP_ITEM );
						-- _choice;
					}
					else {
						_choice = itemCount() - 1;
						driveMenu( REQ_LAST_ITEM );
					}
					break;
				case _KEY_ESCAPE:
					throw EscapePressedException();
				case _KEY_CTRL_C:
					throw ControlCPressedException();
			}
			WindowStack::instance.refresh();
			input = _window.inputCharacter();
		}
		return _choice;
	}

	void show()
	{
		ASSERT( _menu != NULL );
		if ( ! _closed ) {
			LOGGING_WARNING(( "attempted to show unclosed menu '%s'", _window.title() ));
			return;
		}
		_choice = 0;
		int result = post_menu( _menu );
		checkForError( result, "could not post menu" );
		_closed = false;
		_window.show();
		WindowStack::instance.refresh();
	}

	void close()
	{
		ASSERT( _menu != NULL );
		if ( _closed ) {
			LOGGING_WARNING(( "attempted to close an already closed menu '%s'", _window.title() ));
			return;
		}
		int result = unpost_menu( _menu );
		checkForError( result, "could not unpost menu" );
		_closed = true;
		_window.close();
		WindowStack::instance.refresh();
	}

private:
	typedef ITEM * 				ITEM_POINTER;

	MENU * 						_menu;
	ITEM_POINTER *				_itemHandles;
	std::vector< std::string >	_itemNames;
	std::vector< std::string >	_itemDescriptions;
	CUI::Window					_window;
	bool						_closed;
	int							_choice; 

	void setupColors( std::string const & item, std::string const & selected )
	{
		int result;
		result = set_menu_back( _menu, Curses::Colors::colorPair( item ) );
		checkForError( result, "could not set menu background" );
		result = set_menu_fore( _menu, Curses::Colors::colorPair( selected ) );
		checkForError( result, "could not set menu foreground" );
	}

	static void checkForError( int result, const char * message )
	{
		if ( result != E_OK )
			throw Exception( Formatter( "Menu: %s (error %d)", message, result ) );
	}

	void createCursesItems( const ItemSpecifications & items )
	{
		_itemHandles = new ITEM_POINTER[ items.size() + 1 ];
		for ( ItemSpecifications::const_iterator i = items.begin(); i != items.end(); ++i ) {
			createItem( i->name, i->description );
		}
		_itemHandles[ items.size() ] = NULL;
	}

	void createItem(	std::string const & name,
						std::string const & description )
	{
		_itemNames.push_back( name );
		_itemDescriptions.push_back( description );
		unsigned last = _itemNames.size() - 1;
		ASSERT( last == _itemDescriptions.size() - 1 );
		_itemHandles[ last ] = new_item( _itemNames[ last ].c_str(), _itemDescriptions[ last ].c_str() );
		Curses::Curses::checkForError( _itemHandles[ last ], "could not create menu item handle" );
	}

	unsigned itemCount() const
	{ 
		ASSERT( _itemNames.size() == _itemDescriptions.size() );
		return _itemDescriptions.size(); 
	}

	void driveMenu( int menuDriverAction )
	{
		int errorCode = menu_driver( _menu, menuDriverAction );
		if ( errorCode != E_OK && errorCode != E_REQUEST_DENIED )
			throw Exception( Formatter( "menu_driver error (error %d)", errorCode ) );
	}
};

} // Menu
} // CUI
#endif // CUI_MENU_MENU_H
