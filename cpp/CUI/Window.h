#ifndef CUI_WINDOW_H
#define CUI_WINDOW_H

#include "CUI/Curses/Window.h"
#include "CUI/WindowStack.h"

namespace CUI
{

class Window {
public:
	explicit Window(	int height,
						int width,
						int startY,
						int startX,
						Curses::Colors::Color const & frameColor = "NORMAL" ) :
		_outerWindow( height, width, startY, startX ),
		_innerWindow(	_outerWindow,
						height - TOP_AND_BOTTOM_ROWS,
						width - LEFT_AND_RIGHT_COLUMNS,
						1,
						1 ),
		_closed( false )
	{
		_outerWindow.setColor( frameColor );
		_outerWindow.box();
		WindowStack::instance.add( * this );
	}

	~Window()
	{
		close();
	}

	void close()
	{
		if ( _closed )
			return;
		_closed = true;
		WindowStack::instance.hide( * this );
	}

	int inputCharacter() { return _innerWindow.inputCharacter(); }
	void enableScrolling() { _innerWindow.enableScrolling(); }
	void disableScrolling() { _innerWindow.disableScrolling(); }
	void title( std::string const & text )
	{ 
		_title = text;
		_outerWindow.print( _title.c_str(), 0, 1 );
	}

	const char * title() const { return _title.c_str(); }

	void print( const char * string )
	{
		_innerWindow.print( string );
	}

	void virtualRefresh()
	{
		_innerWindow.virtualRefresh();
	}

	void putOnTop()
	{
		WindowStack::instance.putOnTop( * this );
	}

	void putOnBottom()
	{
		WindowStack::instance.putOnBottom( * this );
	}

	void hide()
	{
		close();
	}

	void show()
	{
		_closed = false;
		WindowStack::instance.show( * this );
	}

	Curses::Window & outerWindow() { return _outerWindow; }
	Curses::Window & innerWindow() { return _innerWindow; }

	void enableSpecialKeys()
	{
		_innerWindow.enableSpecialKeys();
		_outerWindow.enableSpecialKeys();
	}

	void setColor( Curses::Colors::Color const & color ) { _innerWindow.setColor( color ); }

private:
	Window( const Window & );

	enum {
		TOP_AND_BOTTOM_ROWS = 2,
		LEFT_AND_RIGHT_COLUMNS = 2
	};

	Curses::Window 	_outerWindow;
	Curses::Window 	_innerWindow;
	bool 			_closed;
	std::string		_title;
};

} // CUI
#endif // CUI_WINDOW_H
