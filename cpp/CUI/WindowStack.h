#ifndef CUI_WINDOWSTACK_H
#define CUI_WINDOWSTACK_H

#include <curses.h>
#include "CUI/KeepSorted.h"
#include "CUI/Window.h"

namespace CUI
{
class Window;

class WindowStack {
public:
	void add( Window & window );
	void putOnTop( Window & window );
	void putOnBottom( Window & window );
	void hide( Window & window );
	void show( Window & window );
	void refresh();

	static WindowStack 						instance;
private:
	typedef KeepSorted< Window *, int > 	SortedWindows;
	SortedWindows					 		_sortedWindows;
	int 									_high;
	int 									_low;

	explicit WindowStack() :
		_high( 0 ),
		_low( -1 )
	{
	}

	void virtualRefreshBackgroundWindow()
	{
		Curses::Curses::virtualRefreshMainWindow();
	}
};

} // CUI
#endif // CUI_WINDOWSTACK_H
