from cuipy.binaries import foundation

results = []
foundation.initialize()
def popup( menu ):
	menu.show()
	choice = menu.choice()
	menu.close()
	return choice

menu = foundation.Menu( 10, 30, 3, 3, title = "from python!", items = [ ( 'a', 'the first' ), ( 'b', 'the second' ) ] )
results.append( popup( menu ) )
menu = foundation.Menu( startX = 5, startY = 5, height = 10, width = 30, title = "another from python!", items = [ ( '1.', 'Item one' ), ( '2.', 'Item two' ), ( '3.', 'Item three' ) ] )
results.append( popup( menu ) )
menu = foundation.Menu( 10, 30, 15, 5, "more from python!", items = [ ( '*', 'Girl' ), ( '*', 'Boy' ), ( '*', 'WTF??' ) ] )
results.append( popup( menu ) )
menu = foundation.Menu( 10, 30, 10, 5, "in color!", items = [ ( '*', 'Girl' ), ( '*', 'Boy' ), ( '*', 'WTF??' ) ], 
						frameColor = "YELLOW_ON_BLACK", 
						itemColor = "GREEN_ON_BLACK", 
						selectedColor = "BLACK_ON_GREEN" )
results.append( popup( menu ) )
foundation.finalize()

print 'you chose', results
