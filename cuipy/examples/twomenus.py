from cuipy.binaries import foundation
foundation.initialize()

results = []

menu = foundation.Menu( 10, 25, 5, 5, title = "Main Menu", items = [ 	( '0', 'First Item' ), 	
																		( '1', 'Second Item' ),
																		( '2', 'Another Menu' ) ] )
menu.show()
result = menu.choice()
results.append( result )
if result == 2:
	menu2 = foundation.Menu( 	10, 25, 7, 7, 
								title = "Second Menu", 
								items = [	( 'a', 'Adam' ),
											( 'e', 'Eve' ),
											( 'E', 'Enosh' ) ],
								frameColor = "YELLOW_ON_BLACK" )
	menu2.show()
	result = menu2.choice()
	menu2.close()
	results.append( result )
menu.close()

foundation.finalize()
print results
