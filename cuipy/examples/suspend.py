from cuipy.binaries import foundation
import os
import time
import sys

def count10Seconds():
	for _ in range( 10 ):
		time.sleep( 1 )
		sys.stderr.write( 'counting...\n' )

results = []
foundation.initialize()

items = [ ( '*', 'ls' ), ( '*', 'ls -l' ), ( '*', 'count 10 seconds' ), ( '*', 'quit' ) ]
menu = foundation.Menu( 10, 30, 3, 3, title = "from python!", items = items )
menu.show()
quit = False
while not quit:
	choice = menu.choice()
	foundation.suspend()
	if choice == 0:
		os.system( 'ls' )
		time.sleep( 2 )
	if choice == 1:
		os.system( 'ls -l' )
		time.sleep( 2 )
	if choice == 2:
		count10Seconds()
	if choice == 3:
		quit = True
	foundation.resume()

foundation.finalize()
