import sys

def _say( message ):
	sys.stdout.write( '\n%s\n' % message )
	sys.stdout.flush()

def fileOpen():
	_say( 'file:open' )

def fileSave():
	_say( 'file:save' )

def configureRead():
	_say( 'configure:read' )

def configureWrite():
	_say( 'configure:write' )

def systemRead():
	_say( "system:read" )

def systemWrite():
	_say( "system:write" )
