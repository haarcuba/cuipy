from cuipy.menus import menu
from cuipy.menus import item
from cuipy.menus import exit
import system
import actions

class Configure( menu.Menu ):
	def __init__( self, label ):
		items = [	item.Item( 'r', 'Read configuration', actions.configureRead ),
					item.Item( 'w', 'Write configuration', actions.configureWrite ),
					system.System( 's' ),
					exit.Exit( 'q', 'return...' ) ]
		menu.Menu.__init__( self, label, 'Configure Menu', items )
