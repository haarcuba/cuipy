from cuipy.menus import menu
from cuipy.menus import item
from cuipy.menus import exit
import actions

class System( menu.Menu ):
	def __init__( self, label ):
		items = [	item.Item( 'r', 'Read system', actions.systemRead ),
					item.Item( 'w', 'Write system', actions.systemWrite ),
					exit.Exit( 'q', 'return...' ) ]
		menu.Menu.__init__( self, label, 'System Menu', items )
