from cuipy.menus import menu
from cuipy.menus import item
from cuipy.menus import exit
import actions
import configure

class File( menu.Menu ):
	def __init__( self ):
		items = [	item.Item( 'o', 'Open', actions.fileOpen ),
					item.Item( 's', 'Save', actions.fileSave ),
					configure.Configure( 'c' ),
					exit.Exit( 'q', 'Quit' ) ]
		menu.Menu.__init__( self, None, 'File Menu', items )
