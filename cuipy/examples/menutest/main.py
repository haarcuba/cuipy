import logging
logging.basicConfig( level = logging.DEBUG )

import concretemenus.file
from cuipy.menus.drivers import tty
from cuipy.menus.drivers import colortty
from cuipy.menus.drivers import basic
import cuipy.wrapper
import cuipy.binaries.foundation
import argparse

def cuipyDemo():
	def _demo():
		menu = concretemenus.file.File()
		driver = tty.TTY( menu, 3, 3 )
		driver.execute()
	cuipy.wrapper.wrapper( _demo )

def colorTTYDemo():
	def _demo():
		menu = concretemenus.file.File()
		driver = colortty.ColorTTY( menu, 3, 3 )
		driver.execute()
	cuipy.wrapper.wrapper( _demo )

def basicDemo():
	menu = concretemenus.file.File()
	driver = basic.Basic( menu, logger = logging )
	driver.execute()

if __name__ == '__main__':
	demonatrations = dict( basic = basicDemo, cuipy = cuipyDemo, colortty = colorTTYDemo )
	logging.info( 'starting' )
	parser = argparse.ArgumentParser()
	parser.add_argument( 'type', help = 'must be one of %s' % demonatrations.keys() )
	arguments = parser.parse_args()
	demo = demonatrations[ arguments.type ]
	demo()
