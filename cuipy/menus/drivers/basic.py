from cuipy.menus import menu
from cuipy.menus import exit
import cuipy.menus.item

class Basic( object ):
	def __init__( self, menu, logger = None ):
		self._menu = menu
		self._fromLabel = {}
		self._logger = logger
		for item in menu.items():
			self._fromLabel[ item.label() ] = item

	def _show( self ):
		print '--- %s ---' % self._menu.description()
		for item in self._menu.items():
			print '%s - %s' % ( item.label(), item.description() )

	def execute( self ):
		while True:
			self._show()
			print "enter your choice: ",
			input = raw_input()
			try:
				item = self._fromLabel[ input ]
				self._executeItem( item )
			except exit.ExitException:
				return
			except Exception as e:
				if self._logger is not None:
					self._logger.exception( "exception happened while executing menu" )
				print 'invalid input. choices are %s' % [ item.label() for item in self._menu.items() ]

	def _executeItem( self, item ):
		if isinstance( item, menu.Menu ):
			driver = Basic( item )
			driver.execute()
		if isinstance( item, cuipy.menus.item.Item ):
			item.execute()
