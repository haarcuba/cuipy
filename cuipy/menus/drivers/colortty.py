from cuipy.menus.drivers import ttymenu
from cuipy.menus.drivers import tty
from cuipy.menus.drivers import menucolors

class ColorTTY( tty.TTY ):
	_menuColors = menucolors.MenuColors()

	def __init__( self, menu, startY, startX ):
		self._menuColors.inspect( menu )
		tty.TTY.__init__( self, menu, startY, startX, ttyMenuFactory = self._factory )

	def _factory( self, menu, startY, startX ):
		return ttymenu.TTYMenu( menu, startY, startX, frameColor = self._menuColors.get( menu ) )
