import itertools
import cuipy.menus.menu

class MenuColors( object ):
	_COLORS = itertools.cycle( [ "WHITE_ON_BLACK", "CYAN_ON_BLACK", "YELLOW_ON_BLACK", "GREEN_ON_BLACK" ] )
	def __init__( self ):
		self._color = None

	def inspect( self, menu ):
		if self._color is not None:
			return
		self._color = {}
		self._walkMenus( menu )

	def _walkMenus( self, menu ):
		self._color[ menu.description() ] = self._COLORS.next()
		for item in menu.items():
			if isinstance( item, cuipy.menus.menu.Menu ):
				self._walkMenus( item )

	def get( self, menu ):
		return self._color[ menu.description() ]
