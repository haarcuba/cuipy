import cuipy.binaries.foundation

class TTYMenu( object ):
	def __init__( self, menu, startY, startX, frameColor = "WHITE_ON_BLACK" ):
		TOP_LINE = BOTTOM_LINE = 1
		height = len( menu.items() ) + TOP_LINE + BOTTOM_LINE
		WIDTH_SLACK = 7
		displayedStrings = [ len( item.description() ) for item in menu.items() ] + [ len( menu.description() ) ]
		width = max( displayedStrings ) + WIDTH_SLACK
		items = [ ( ' ', item.description() ) for item in menu.items() ]
		self._ttyMenu = cuipy.binaries.foundation.Menu( height, width, startY, startX,
														title = str( menu.description() ),
														items = items, 
														frameColor = frameColor )
		self._coordinates = startY, startX

	def coordinates( self ):
		return self._coordinates

	def show( self ):
		self._ttyMenu.show()

	def close( self ):
		self._ttyMenu.close()

	def choice( self ):
		return self._ttyMenu.choice()
