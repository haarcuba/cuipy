from cuipy.menus import menu
from cuipy.menus import exit
import cuipy.menus.item
from cuipy.menus.drivers import ttymenu
import sys
import logging

class TTY( object ):
	def __init__( self, menu, startY, startX, ttyMenuFactory = ttymenu.TTYMenu ):
		self._menu = menu
		self._ttyMenuFactory = ttyMenuFactory
		self._ttyMenu = self._ttyMenuFactory( self._menu, startY, startX )

	def execute( self ):
		stop = False
		self._ttyMenu.show()
		while not stop:
			try:
				choice = self._ttyMenu.choice()
				item = self._menu.items()[ choice ]
				self._executeItem( item )
			except exit.ExitException:
				stop = True
			except cuipy.binaries.foundation.Menu.EscapePressedException:
				stop = True
			except Exception as e:
				logging.exception( 'while executing menu %s' % self._menu.description() )
				raise
				
		self._ttyMenu.close()

	def _executeItem( self, item ):
		if isinstance( item, menu.Menu ):
			startY, startX = self._ttyMenu.coordinates()
			driver = TTY( item, startY + 1, startX + 1, self._ttyMenuFactory )
			driver.execute()
		if isinstance( item, cuipy.menus.item.Item ):
			cuipy.binaries.foundation.suspend()
			item.execute()
			self._allowUserToReviewOutput()
			cuipy.binaries.foundation.resume()
	
	def _allowUserToReviewOutput( self ):
		sys.stdout.write( '\npress enter to go back...\n' )
		sys.stdout.flush()
		raw_input()
