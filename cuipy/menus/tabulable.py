class Tabulable( object ):
	def __init__( self, label, description ):
		self._label = label
		self._description = description

	def label( self ):
		return self._label

	def description( self ):
		return self._description
