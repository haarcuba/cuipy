from cuipy.menus import tabulable

class Menu( tabulable.Tabulable ):
	def __init__( self, label, description, items ):
		self._items = items
		tabulable.Tabulable.__init__( self, label, description )

	def items( self ):
		return self._items
