from cuipy.menus import item
class ExitException( Exception ): pass

class Exit( item.Item ):
	def __init__( self, label, description ):
		item.Item.__init__(	self,
							label,
							description,
							action = self._exit )

	def _exit( self ):
		raise ExitException()
