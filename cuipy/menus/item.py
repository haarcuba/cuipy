from cuipy.menus import tabulable

class Item( tabulable.Tabulable ):
	def __init__( self, label, description, action ):
		self._action = action
		tabulable.Tabulable.__init__( self, label, description )

	def execute( self ):
		return self._action()
