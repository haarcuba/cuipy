import cuipy.binaries.foundation

def wrapper( function ):
	cuipy.binaries.foundation.initialize()
	try:
		function()
	finally:
		cuipy.binaries.foundation.finalize()	
